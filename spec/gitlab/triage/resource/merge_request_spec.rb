# frozen_string_literal: true

require 'spec_helper'

require 'gitlab/triage/resource/merge_request'

describe Gitlab::Triage::Resource::MergeRequest do
  include_context 'with network context'

  it_behaves_like 'issuable'

  describe '#reference' do
    subject { described_class.new({}) }

    it { expect(subject.reference).to eq('!') }
  end

  describe '#first_contribution?' do
    include_context 'with stubs context'

    subject { described_class.new(mr.with_indifferent_access, network: network) }

    context 'when first_contribution is not loaded' do
      it 'returns the value from the API' do
        stub_get = stub_api(:get, subject.__send__(:resource_url)) do
          { first_contribution: true }
        end

        expect(subject.first_contribution?).to eq(true)

        assert_requested(stub_get)
      end
    end

    context 'when first_contribution is loaded' do
      before do
        mr[:first_contribution] = false
      end

      it 'returns the value directly' do
        expect(subject.first_contribution?).to eq(false)
      end
    end
  end

  describe '#url' do
    subject { described_class.new(resource, network: network).__send__(:url) }

    let(:resource) { { source_id_name => 123 } }

    context 'when source is project' do
      let(:source_id_name) { :project_id }

      it 'returns the url pointing to the current resources' do
        expect(subject).to eq(
          "#{base_url}/projects/123/merge_requests?per_page=100")
      end
    end

    context 'when source is group' do
      let(:source_id_name) { :group_id }

      it 'returns the url pointing to the current resources' do
        expect(subject).to eq(
          "#{base_url}/groups/123/merge_requests?per_page=100")
      end
    end
  end
end
