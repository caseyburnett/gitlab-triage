module Gitlab
  module Triage
    module Errors
      module Network
        InternalServerError = Class.new(StandardError)
      end
    end
  end
end
