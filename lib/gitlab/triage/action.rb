require_relative 'action/summarize'
require_relative 'action/comment'

module Gitlab
  module Triage
    module Action
      def self.process(policy:, **args)
        {
          Summarize => policy.summarize?,
          Comment => policy.comment?
        }.compact.each do |action, active|
          act(action: action, policy: policy, **args) if active
        end
      end

      def self.act(action:, dry:, **args)
        klass =
          if dry
            action.const_get(:Dry)
          else
            action
          end

        klass.new(**args).act
      end
    end
  end
end
