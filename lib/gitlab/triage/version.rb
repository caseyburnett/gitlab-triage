# frozen_string_literal: true

module Gitlab
  module Triage
    VERSION = '1.14.3'
  end
end
